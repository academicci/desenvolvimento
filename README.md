# Leia-me: Projeto Academicci
## Objetivo
Descrição resumida do sistema

## Estrutura do Diretório 
|Caminho| Conteúdo
|-|-|
|.|README|
|.academicci_source/|Código-fonte do sistema|
**OBS.:** Inserir diretório de código.


# Diretrizes de Commit
### Objetivo
Criar uma padronização no envio de commits para o repositório.

### Diretrizes Gerais
1. Antes de realizar qualquer alteração no repositório local, obter as atualizações do repositório distribuído.  
Comando no bash: `git pull origin <nome da branch>`
2. Não colocar acentuação nas mensagens de commit. 
3. É opcional inserir uma descrição para um commit, além da sua mensagem.  
Comando no bash: `git commit -m <Mensagem do Commit> -m <Descrição OPCIONAL do Commit>`
4. Ao atualizar um artefato de documentação (documento de visão, regras de negócio, etc) padronizar o commit da seguinte forma: `Versao <número da versão> <Nome do Documento>`  
Exemplo: `Versao 0.005 Documento de Visao` 

## Git Flow 
Comandos do Git Flow disponíveis neste link: [GIT FLOW](https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html)
